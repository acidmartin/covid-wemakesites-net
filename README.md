# covid-wemakesites-net

> Real-time Coronavirus COVID-19 global cases by Martin Ivanov and data API provided by lmao.ninja.

## Setting-up the Cronjob

> */15	*	*	*	*	/opt/cpanel/ea-php72/root/usr/bin/php /home/wemakesites/bg-rock-archives.wemakesites.net/wake-up.php >/dev/null 2>&1

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

## Deployment

> Deploy the contents of the dist/ folder onto the server. The api/ folder
  is not included in the build, so if changes were made to that code it needs
  to be deployed manually

## Todos

- [done] Send events to g-tag
- [done] Publicize:
  - [done] Write a a blog post
  - [done] Submit the project to MadeWithVueJs abd VuejsFeed
  - [done] Facebook
  - [done] Tweet
- [done] Export to:
  - [done] .png
  - [done] .json
  - [done] .csv
- [done] (1.0.1) Add cookie consent bar
- [done] (1.0.1) Case Fatality Rate
- [done] (1.0.1) Format the numbers
- [done] Add to Google Search Console
- [done] Fix the "NavigationDuplicated" error
- [done] Document title for each page
- [done] Add aria-label all for buttons
- [done] Implement real-time map updates
- [done] Add charts
- [done] Document the code
- [done] Add:
  - [done] /robots.txt,
  - [done] /sitemap.xml
  - [done] /.htaccess (cache, mod_rewrite, g-zip)
  - [done] /.favicon and application icons
  - [done] /static/data/manifest.json
  - [done] Add content to the "keywords" and "description" meta-tags
  - [done] Add the necessary meta-tags
  - [done] Add the noscript tag
- [done] Move covid.wemakesites.net/api to this project  
- [done] Dark and light CSS scrollbars for the countries list
- [done] Cronjob to get the latest data from the server
- [done] Add Google Tag Manager (npm vue-gtag)  
- [done] Toolbar buttons and routes:
  - [done] /today/all
  - [done] /about
  - [done] Calendar for historic data
  - [done] On the /about age add the external-links component
  - [won't do]/help (include the app-controls component)  
- [done] Set selected link color in the countries list according to the color scheme selected  
- [done] Fix the footer texts  
- [done] Find a solution for the auto height of the countries list
- [done] Complete README.md file
- [done] Display the current date some place visible
- [done] Credit lmao.ninja
- [done] Add Axios interceptors
- [done] Events for ajax - start, end, error
- [done] Loading overlay in the Axios interceptops
- [done] Handle HTTP and JavaScript errors due to missing data
- [done] Fix errors with empty coords, today and countries data in $root
- [done] If /:date/:country is "all" do not get geo, show world map
- [done] Handle null in today, history or coords in $root
- [done] Red and green color for the deaths, total deaths and recovered in the country panel
- [done] Fix countries like USA, etc in /:date/:country
- [done] Use differently sized circles for the markers
- [done] Handle error in /date/non-existing-country
- [done] Store historical data on the server
- [done] Countries filter
- [done] Smaller font-size for totals, so they don't wrap (or check the wrap property of the grid)
- [done] Add Leaflet map
- [won't do] Page component for additional page info
- [won't do] Use a dark overlay for the map if available
- [won't do] (1.0.2) Show closable alert for small screens to use the menu on the left
- [wot't do] (1.0.2) Add export button in the countries-list component
- [won't do] (1.0.2) Center the map for global view
