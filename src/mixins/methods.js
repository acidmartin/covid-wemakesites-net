/**
 * Methods mixin, available application-wide
 * @module methods
 */

import countryInfoFixes from '../helpers/country-info-fixes'
import numeral from 'numeral'

const methods = {
  methods: {
    /**
     * Format number to human readable
     * @method formatNumber
     * @param {number} number [number = 0]
     * @return {string}
     */
    formatNumber (number = 0) {
      return numeral(number).format(this.$config.numeralsFormat)
    },
    /**
     * Shortcut to window.open
     * @method getUrl
     * @param {string} url [url = '']
     * @param {string} target [target = '_self']
     * @return {void}
     */
    getUrl (url = '', target = '_self') {
      window.open(this.$config.donateUrl, target)
    },
    /**
     * Set donut hole color according to the theme
     * @method setDonutHoleColor
     * @return {string}
     */
    setDonutHoleColor () {
      return this.$root.config.dark ? '#424242' : '#fff'
    },
    /**
     * Get country info from the API
     * @method getCountryInfo
     * @param {string} country [country = '']
     * @return {void}
     */
    getCountryInfo (country = '') {
      if (!country) {
        return
      }
      const route = this.$api.country
      this.$root.loadingOverlayMessage = this.getLoadingOverlayMessage('getCountryInfo')
      country = country.toLowerCase()
      country = countryInfoFixes[country] || country
      this.$http({
        url: route.url,
        method: route.methods[0],
        data: {
          country
        }
      }).then(response => {
        if (response.data && response.data.length) {
          this.$root.$emit(this.$events.country, response.data[0])
          this.$root.loadingOverlayMessage = ''
        }
      }).catch(error => {
        console.log(error)
      })
    },
    /**
     * Send event to Gtag
     * @method gtEvent
     * @param {string} action
     * @param {object} data
     * {
     *  event_category: {string}
     *  event_label: {string}
     * }
     * @return {string}
     */
    gtEvent (action, data) {
      this.$gtag.event(action, data)
    },
    /**
     * Get an overlay message from the language file
     * @method getLoadingOverlayMessage
     * @param {string} message [message = '']
     * @return {string}
     */
    getLoadingOverlayMessage (message = '') {
      return this.$lang.loadingOverlayMessages[message] || null
    },
    /**
     * Add or remove bookmarked location
     * @method bookmark
     * @param {string} country [country = '']
     * @return {void}
     */
    bookmark (country = '') {
      if (!country) {
        return
      }
      const bookmarks = this.$root.bookmarks
      const bookmarksLocalStorageKey = this.$config.bookmarksLocalStorageKey
      if (!bookmarks.includes(country)) {
        bookmarks.push(country)
        this.gtEvent('click', {
          event_category: 'add bookmark',
          event_label: country
        })
      } else {
        const index = bookmarks.findIndex(item => item === country)
        bookmarks.splice(index, 1)
        this.gtEvent('click', {
          event_category: 'remove bookmark',
          event_label: country
        })
      }
      localStorage.setItem(bookmarksLocalStorageKey, JSON.stringify(bookmarks))
      if (!bookmarks.length && this.bookmarksDialog) {
        this.bookmarksDialog = false
      }
    },
    /**
     * Return new Date() one month from now
     * @method oneMonthFromNow
     * @return {Date}
     */
    oneMonthFromNow () {
      return new Date(Date.now() + 2.628e+9)
    },
    /**
     * Create a file name for the export buttons
     * @method makeFileName
     * @param {string} name [name = this.$root.config.today]
     * @param {string} format [format = 'png']
     * @return {string}
     */
    makeFileName (name = this.$root.config.today, format = 'png') {
      return `${this.$root.config.today.replace(/\//g, '-')}.${format}`
    },
    /**
     * Shortcut to window.location.reload
     * @method locationReload
     * @return {void}
     */
    locationReload () {
      location.reload()
    },
    /**
     * Get color from the current color scheme
     * @method getColorScheme
     * @param {string} color [getColorScheme = 'primary']
     * @param {boolean} text [text = false]
     * @return {string}
     */
    getColorScheme (color = 'primary', text = false) {
      const root = this.$root
      const config = root.config
      const colors = root.colors
      const colorScheme = colors[config.colorScheme] || colors[config.defaultColorScheme]
      if (!colors[config.colorScheme]) {
        this.setColorScheme(colorScheme.primary)
      }
      return `${colorScheme[color]}${text ? '--text' : ''}`
    },
    /**
     * Set color scheme and store locally
     * @method setColorScheme
     * @param {string} color [color=config.defaultColorScheme]
     * @return {void}
     */
    setColorScheme (color = this.$root.config.defaultColorScheme) {
      this.$root.config.colorScheme = color
      localStorage.setItem('scheme', color)
      this.gtEvent('click', {
        event_category: 'set color scheme',
        event_label: color
      })
    },
    /**
     * Get country data by name
     * @method getCountryByName
     * @param {string} country [country = this.$route.params.country]
     * @return {object}
     */
    getCountryByName (country = this.$route.params.country) {
      return this.$root.countries.find(item => item.country.toLowerCase() === country.toLowerCase())
    },
    /**
     * Get country geo data by name
     * @method getCountryGeoData
     * @param {string} country [country = this.$route.params.country]
     * @return {object}
     */
    getCountryGeoData (country = this.$route.params.country) {
      const coords = this.$root.coords
      const index = coords.findIndex(item => item[3].toLowerCase() === country.toLowerCase())
      return coords[index] || null
    },
    /**
     * Get country geo data by name
     * @method getCountryGeoData
     * @param {string} country [country = 'all']
     * @param {string} date [date = this.$root.$config.today]
     * @return {object}
     */
    getCountryByDate (country = 'all', date = this.$root.$config.today) {
      return `/${date}/${country.toLowerCase()}`
    },
    /**
     * Set document.title
     * @method setDocumentTitle
     * @param {string} subtitle [subtitle = '']
     * @param {string} delimiter [delimiter = '|']
     * @return {void}
     */
    setDocumentTitle (subtitle = '', delimiter = ' | ') {
      document.title = `${this.$config.appTitle}${subtitle ? delimiter : ''}${subtitle}`
    }
  }
}

export default methods
