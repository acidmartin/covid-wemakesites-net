import Vue from 'vue'
import App from './App'
import Vuetify from 'vuetify'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueGtag from 'vue-gtag'
import numeral from 'numeral'
import numFormat from 'vue-filter-number-format'

import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'material-icons/iconfont/material-icons.css'

import router from './helpers/router'
import api from './helpers/api'
import config from './helpers/config'
import colors from './helpers/colors'
import lang from './helpers/lang'
import countryNameFixes from './helpers/country-name-fixes'
import events from './helpers/events'

import methods from './mixins/methods'

import CountriesList from './components/countries-list'
import AppAvatar from './components/app-avatar'
import AppControls from './components/app-controls'
import Leafmap from './components/leafmap'
import Total from './components/total'
import CountryTotals from './components/country-totals'
import LoadingOverlay from './components/loading-overlay'
import ExternalLinks from './components/external-links'
import LineChart from './components/line-chart'
import CompareDates from './components/compare-dates'
import VcStyle from './components/vc-style'
import VcPieChart from './components/vc-pie-chart'
import Cfr from './components/cfr'
import ExportCsv from './components/export-csv'
import ExportJson from './components/export-json'
import ExportImage from './components/export-image'
import ExportAsLabel from './components/export-as-label'
import CookieConsent from './components/cookie-consent'
import CfrGrid from './components/cfr-grid'
import StatsNav from './components/stats-nav'
import NewsFeed from './components/news-feed'
import Country from './components/country'
import ApiListing from './components/api-listing'
import Continent from './components/continent'
import States from './components/states'
import State from './components/state'
import BuyMeACoffee from './components/buy-me-a-coffee'
import Projections from './components/projections'

Vue.use(Vuetify)
Vue.use(VueAxios, axios)
Vue.use(VueGtag, {
  config: {
    id: config.googleAnalyticsId,
    params: {
      send_page_view: false
    }
  }
}, router)

Vue.filter('numFormat', numFormat(numeral))

Vue.component('countries-list', CountriesList)
Vue.component('app-avatar', AppAvatar)
Vue.component('app-controls', AppControls)
Vue.component('leafmap', Leafmap)
Vue.component('total', Total)
Vue.component('country-totals', CountryTotals)
Vue.component('loading-overlay', LoadingOverlay)
Vue.component('external-links', ExternalLinks)
Vue.component('line-chart', LineChart)
Vue.component('compare-dates', CompareDates)
Vue.component('vc-style', VcStyle)
Vue.component('vc-pie-chart', VcPieChart)
Vue.component('cfr', Cfr)
Vue.component('export-csv', ExportCsv)
Vue.component('export-json', ExportJson)
Vue.component('export-image', ExportImage)
Vue.component('export-as-label', ExportAsLabel)
Vue.component('cookie-consent', CookieConsent)
Vue.component('cfr-grid', CfrGrid)
Vue.component('stats-nav', StatsNav)
Vue.component('news-feed', NewsFeed)
Vue.component('country', Country)
Vue.component('api-listing', ApiListing)
Vue.component('continent', Continent)
Vue.component('states', States)
Vue.component('state', State)
Vue.component('buy-me-a-coffee', BuyMeACoffee)
Vue.component('projections', Projections)

Vue.prototype.$api = api
Vue.prototype.$config = config
Vue.prototype.$lang = lang
Vue.prototype.$events = events

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  router,
  el: '#app',
  components: {
    App
  },
  mixins: [
    methods
  ],
  data () {
    return {
      config,
      colors,
      countryNameFixes,
      today: {},
      countries: [],
      coords: [],
      history: [],
      states: [],
      location: [],
      ready: false,
      firstRun: true,
      noDataAvailable: false,
      datePickerDate: null,
      datePickerDialog: false,
      compareDates: {},
      bookmarks: [],
      noScroll: false,
      noAjaxSpinner: false,
      newsLoading: false,
      loadingOverlayMessage: 'tralala'
    }
  },
  template: '<App/>',
  watch: {
    /**
     * @watcher
     * @return {void}
     */
    'config.today' () {
      this.getHistory()
    },
    /**
     * @watcher
     * @return {void}
     */
    '$route.path' () {
      window.scrollTo(0, 0)
      this.datePickerDialog = false
    }
  },
  created () {
    this.onCreated()
  },
  methods: {
    /**
     * Handler for the $created lifecycle event of the component
     * @method onCreated
     * @return {void}
     */
    onCreated () {
      const rtm = this.config.realTimeUpdates
      const bookmarks = localStorage.getItem(this.$config.bookmarksLocalStorageKey)
      this.addAxiosInterceptors()
      this.setDocumentTitle()
      this.getData()
      if (rtm && parseInt(rtm, 10)) {
        setInterval(() => {
          this.getData()
        }, rtm * 60000)
      }
      this.bookmarks = bookmarks ? JSON.parse(bookmarks) : []
    },
    /**
     * Reset critical data
     * @method resetData
     * @return {void}
     */
    resetData () {
      this.today = {}
      this.countries = []
      this.coords = []
      this.history = []
    },
    /**
     * Bootstrap the data
     * @method getData
     * @return {void}
     */
    getData () {
      this.resetData()
      this.$nextTick(() => {
        this.getCompareDates()
      })
    },
    /**
     * Set Axios interceptors
     * @method addAxiosInterceptors
     * @return {void}
     */
    addAxiosInterceptors () {
      const events = this.$events
      const ajax = events.ajax
      const interceptors = this.$http.interceptors
      interceptors.request.use(config => {
        this.$emit(ajax, true)
        return config
      }, function (error) {
        this.$emit(ajax, false)
        return Promise.reject(error)
      })

      interceptors.response.use(response => {
        if (!this.noScroll) {
          window.scrollTo(0, 0)
        }
        this.$emit(ajax, false)
        return response
      }, (error) => {
        this.$emit(ajax, false)
        this.$emit(events.error, false)
        return Promise.reject(error)
      })
    },
    /**
     * Get the dates array
     * @method getCompareDates
     * @return {void}
     */
    getCompareDates () {
      const route = this.$api['compare/dates']
      this.loadingOverlayMessage = this.getLoadingOverlayMessage('compareDates')
      this.$http({
        url: route.url,
        method: route.methods[0]
      }).then(response => {
        if (response.data && response.status === 200) {
          this.compareDates = response.data
          if (!this.compareDates || !Object.keys(this.compareDates).length) {
            this.noDataAvailable = true
            return
          }
          this.getHistory()
        }
      }).catch(error => {
        console.error(error)
      })
    },
    /**
     * Get the array of available dates of historical data
     * @method getHistory
     * @return {void}
     */
    getHistory () {
      const route = this.$api.history
      this.loadingOverlayMessage = this.getLoadingOverlayMessage('getHistory')
      this.$http({
        url: route.url,
        method: route.methods[0]
      }).then(response => {
        if (response.data && response.status === 200) {
          this.history = response.data
          if (!this.history.length) {
            this.noDataAvailable = true
            return
          }
          this.getToday()
        }
      }).catch(error => {
        console.error(error)
      })
    },
    /**
     * Get the data for the selected date
     * @method getToday
     * @return {void}
     */
    getToday () {
      const route = this.$api.date
      this.loadingOverlayMessage = this.getLoadingOverlayMessage('getToday')
      this.$http({
        url: route.url,
        method: route.methods[0],
        data: {
          date: this.$config.today
        }
      }).then(response => {
        if (response.data && response.status === 200) {
          const countryNameFixes = this.countryNameFixes
          this.today = response.data.all
          if (!this.today) {
            this.noDataAvailable = true
            return
          }
          if (response.data.countries && response.data.countries.length) {
            Object.keys(countryNameFixes).forEach(name => {
              const country = response.data.countries.find(item => item.country === name)
              if (country) {
                country.country = countryNameFixes[name]
              }
            })
          }
          this.countries = response.data.countries
          if (!this.countries || !this.countries.length) {
            this.noDataAvailable = true
            return
          }
          this.$nextTick(() => {
            this.getCoords()
          })
        }
      }).catch(error => {
        console.error(error)
      })
    },
    /**
     * Get the geo data for all countries
     * @method getCoords
     * @return {void}
     */
    getCoords () {
      this.loadingOverlayMessage = this.getLoadingOverlayMessage('getCoords')
      const route = this.$api.coords
      this.$http({
        url: route.url,
        method: route.methods[0]
      }).then(response => {
        if (response.data && response.status === 200) {
          this.coords = response.data
          if (!this.coords || !this.coords.length) {
            this.noDataAvailable = true
            return
          }
          this.loadingOverlayMessage = this.getLoadingOverlayMessage('ready')
          this.datePickerDate = new Date(this.config.today).toISOString().substr(0, 10)
          this.ready = true
          this.$emit(this.$events.update)
        }
      }).catch(error => {
        console.error(error)
      })
    }
  }
})
