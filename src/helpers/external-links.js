/**
 * External links
 * @module externalLinks
 */

const externalLinks = [{
  url: 'https://vueribbon.com/home',
  title: 'VueRibbon',
  description: 'MS Office ribbon component for Vue. One of my favorite projects, started back in 2010 as a jQuery plugin.'
}, {
  url: 'https://editor.vueribbon.com/',
  title: 'WYSIWYG Editor with VueRibbon',
  description: 'Demo WYSIWYG editor built with my VueRibbon component.'
}, {
  url: 'https://csshdr.wemakesites.net/',
  title: 'CSS HDR Editor',
  description: 'HDR photo editor, running fully on the client side and using CSS filters to create and edit images and also export and import them as JSON or JPG.'
}, {
  url: 'https://bg-rock-archives.wemakesites.net/',
  title: 'BG Rock Archives',
  description: 'Unofficial music website for Bulgarian rock/metal bands, using NodeJs and Heroku for scraping data from the old "official" website. Might be a bit slow as I am using the free Heroku dinos and they might be asleep.'
}, {
  url: 'https://npg.wemakesites.net',
  title: 'CoinDesk Data',
  description: 'Historical BitCoin data from CoinDesk'
}, {
  url: 'https://vuecidity.wemakesites.net/',
  title: 'Vuecidity',
  description: 'UI library for Vue based on Google Material, my first Vue attempt.'
}, {
  url: 'https://vc-chord-diagram.wemakesites.net/',
  title: 'Chord Diagrams',
  description: 'Chord diagrams component built with Vue and SVG.'
}, {
  url: 'https://parallax.wemakesites.net/',
  title: 'Vue Parallax',
  description: 'Parallax component for Vue for single page websites.'
}, {
  url: 'https://vc-piechart.wemakesites.net/',
  title: 'Vue PieChart',
  description: 'Pie and donut chart component for Vue, and experiment with CSS conic gradients.'
}, {
  url: 'https://martinivanov.net/',
  title: 'My Blog',
  description: 'My WordPress blog running on a custom-built Vue/Vuetify front-end.'
}, {
  url: 'https://wemakesites.net/',
  title: 'My Personal Website and Portfolio',
  description: 'My portfolio website running on a custom PHP backend and Vue/Vuetify front-end.'
}]

export default externalLinks
