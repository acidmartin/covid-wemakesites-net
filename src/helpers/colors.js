/**
 * Application colors
 * @module colors
 */

import colorSchemes from './color-schemes'
const hexDanger = '#d50000'
const hexSuccess = '#4caf50'
const hexWarning = '#ff9800'

const colors = {
  hexDanger,
  hexSuccess,
  hexWarning,
  ...colorSchemes,
  active: '#ff5722',
  critical: '#ffeb3b',
  todayCases: '#ff6d00',
  todayDeaths: '#f44335'
}

export default colors
