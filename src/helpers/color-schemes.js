/**
 * Application color schemes
 * @module colorSchemes
 */

const colorSchemes = {
  red: {
    primary: 'red',
    danger: 'red',
    success: 'green',
    warning: 'warning'
  },
  pink: {
    primary: 'pink',
    danger: 'red',
    success: 'green',
    warning: 'warning'
  },
  'deep-purple': {
    primary: 'deep-purple',
    danger: 'red',
    success: 'green',
    warning: 'warning'
  },
  indigo: {
    primary: 'indigo',
    danger: 'red',
    success: 'green',
    warning: 'warning'
  },
  green: {
    primary: 'green',
    danger: 'red',
    success: 'green',
    warning: 'warning'
  },
  orange: {
    primary: 'orange',
    danger: 'red',
    success: 'green',
    warning: 'warning'
  }
}

export default colorSchemes
