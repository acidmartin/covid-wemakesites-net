/**
 * Map of country name versus api/country data discrepancies
 * @module countryInfoFixes
 */

const countryInfoFixes = {
  'united states': 'usa'
}

export default countryInfoFixes
