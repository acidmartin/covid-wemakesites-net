/**
 * Application router
 * @module Router
 */

import Vue from 'vue'
import Router from 'vue-router'
import Countries from '../pages/countries'
import Index from '../pages/index'
import About from '../pages/about'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [{
    path: '/',
    name: 'index',
    component: Index
  }, {
    path: '/:date/:country',
    name: 'countries',
    component: Countries
  }, {
    path: '/about',
    name: 'about',
    component: About
  }]
})
