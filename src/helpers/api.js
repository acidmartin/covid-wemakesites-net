/**
 * API route definitions
 * @module api
 */

import config from './config'
import api from '../../api/routes'

const apiRoot = config.apiRoot

Object.keys(api).forEach(route => {
  api[route].url = apiRoot + api[route].url
})

export default api
