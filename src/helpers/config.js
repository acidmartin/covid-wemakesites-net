/**
 * Application configuration
 * @module config
 */

import {
  author,
  version,
  appTitle,
  lastUpdated,
  description
} from '../../package'
import moment from 'moment'
import colors from './colors'

const port = 8888
const defaultColorScheme = 'red'
const dark = localStorage.getItem('dark')
const routeFormat = 'YYYY-MM-DD'
const mapboxAccessToken = 'pk.eyJ1IjoiYWNpZG1hcnRpbiIsImEiOiJjaml5N3djbW4wN3BqM3JwbDIxa2YwYWtsIn0.SDr95iYmYOjIN6X9vbsjMA'
const latitude = 42.798882
const longitude = 23.431556
const hexDanger = colors.hexDanger
const hexSuccess = colors.hexSuccess
const hexWarning = colors.hexWarning
const apiRoot = window.location.href.includes('localhost') ? `http://localhost:${port}/covid-wemakesites-net/api/` : 'https://covid.wemakesites.net/api/'
const today = moment(new Date()).format(routeFormat)
const colorScheme = localStorage.getItem('scheme') || defaultColorScheme

const config = {
  today,
  author,
  apiRoot,
  version,
  appTitle,
  hexDanger,
  hexSuccess,
  hexWarning,
  description,
  routeFormat,
  lastUpdated,
  colorScheme,
  defaultColorScheme,
  donateButton: true,
  realTimeUpdates: 15,
  dataLastUpdated: 'lll',
  numeralsFormat: '0,0',
  appIcon: 'mdi-chart-bar',
  googleAnalyticsId: 'UA-17572030-35',
  dark: dark ? JSON.parse(dark) : true,
  humanReadableDateFormat: 'ddd, MMM D',
  bookmarksLocalStorageKey: 'bookmarks',
  donateUrl: 'https://buymeacoff.ee/acidmartin',
  caseColors: {
    cases: hexWarning,
    deaths: hexDanger,
    recovered: hexSuccess,
    active: colors.active,
    critical: colors.critical,
    todayCases: colors.todayCases,
    todayDeaths: colors.todayDeaths
  },
  map: {
    latitude,
    longitude,
    mapboxAccessToken,
    enabled: true,
    width: '100%',
    height: '75vh',
    marker: {
      stroke: 0,
      fillOpacity: '.65',
      fillColor: hexDanger
    },
    markers: [{
      latitude,
      longitude
    }]
  },
  excludedLocations: [
    'World'.toLowerCase()
  ]
}

export default config
