/**
 * Map of country name versus api/data/coords.json data discrepancies
 * @module countryNameFixes
 */

const countryNameFixes = {
  'S. Korea': 'South Korea',
  'USA': 'United States',
  'UK': 'United Kingdom',
  'UAE': 'United Arab Emirates',
  'Czechia': 'Czech Republic',
  'Palestine': 'Palestinian Territories',
  'Macao': 'Macau',
  'Faeroe Islands': 'Faroe Islands',
  'Saint Martin': '',
  'St. Barth': 'Saint Barthélemy',
  'DRC': 'Democratic Republic of the Congo',
  'St. Vincent Grenadines': 'Saint Vincent and the Grenadines',
  'Eswatini': 'Swaziland',
  'CAR': 'Central African Republic',
  'Iran, Islamic Republic of': 'Iran',
  'Libyan Arab Jamahiriya': 'Libya',
  'Turks and Caicos Islands': 'Turks and Caicos',
  'Holy See (Vatican City State)': 'Vatican City',
  'Syrian Arab Republic': 'Syria',
  'Lao People\'s Democratic Republic': 'Laos',
  'Tanzania, United Republic of': 'Tanzania',
  'Congo, the Democratic Republic of the': 'Democratic Republic of the Congo',
  'Moldova, Republic of': 'Moldova',
  'Viet Nam': 'Vietnam',
  'Venezuela, Bolivarian Republic of': 'Venezuela',
  'Coast D\'Ivoire': 'Ivory Coast',
  'Palestinian Territory, Occupied': 'Palestinian Territories',
  'Diamond Princess': 'MS Diamond Princess',
  'Falkland Islands (Malvinas)': 'Falkland Islands',
  'Saint Pierre Miquelon': 'Saint Pierre and Miquelon',
  'Sao Tome and Principe': 'Saint Thomas and Prince'
}

export default countryNameFixes
