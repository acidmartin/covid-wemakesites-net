/**
 * Application custom events map
 * @module colors
 */

const events = {
  ajax: 'ajax',
  error: 'error',
  update: 'update',
  country: 'country',
  close: 'close'
}

export default events
