/**
 * Application localization strings
 * @module config
 */

import numeral from 'numeral'
import config from './config'

const numeralsFormat = config.numeralsFormat
const recovered = 'Recovered'
const confirmedCases = 'Confirmed Cases'
const deaths = 'Total Fatalities'
const donate = 'Buy me a Coffee'

const lang = {
  deaths,
  donate,
  recovered,
  confirmedCases,
  tabDetails: 'Details',
  notAvailable: 'n/a',
  selectChildRegion: 'Select state, region or province',
  tabProjections: 'Projections',
  notImplemented: 'Not Implemented',
  newsAreAvailablePerCountry: 'Please, select a country to view latest news about the Coronavirus spread.',
  travelSeverelyLimited: 'Travel severely limited',
  noProjectionsData: 'Projections data from IHME is not available at the moment for the specified location.',
  version: 'Version',
  api: 'REST API',
  state: 'State',
  tests: 'Tests',
  testsPerOneMillion: 'Tests Per 1 Million',
  byState: 'View pandemic data by US state',
  byContinent: 'View pandemic data by continent',
  continentsDescription: 'The charts below show the current pandemic spread by continent.',
  statesDescription: 'The charts below show the current pandemic spread across the United States. You can filter by state name.',
  apiDescription: 'Below are the available API methods used by this app. They can be used freely, no API key needed at present.',
  of: 'out of',
  callingCodes: 'Calling codes',
  capital: 'Capital city',
  region: 'Region',
  subregion: 'Sub-region',
  population: 'Population',
  latlng: 'Latitude and longitude',
  area: 'Area',
  active: 'Active',
  timezones: 'Time zones',
  borders: 'Borders',
  currencies: 'Currencies',
  languages: 'Languages',
  news: 'News',
  mainStats: 'Main Statistics',
  map: 'Map',
  charts: 'Charts',
  cfrAbbr: 'CFR',
  dismiss: 'Dismiss',
  yesterday: 'Yesterday',
  countriesAffected: 'Locations',
  twitter: 'Follow me on Twitter',
  bookmarkAdd: 'Bookmark location',
  bookmarkRemove: 'Remove bookmark',
  bookmarked: 'bookmarked',
  country: 'Country',
  updated: 'Updated',
  projectedPeakResourceUse: 'Projected resource use',
  bookmarkedLocations: 'Bookmarked Locations',
  globalCasesMovement: 'Global Cases Movement by Date',
  cookieConsentMessage: 'We use cookies to ensure you get the best experience',
  allCountries: 'All Countries',
  countryAutoSelected: 'The default location was automatically set based on your GeoIP data.',
  cfr: 'Case Fatality Rate (CFR) for the selected date',
  cases: 'Cases',
  cancel: 'Cancel',
  close: 'Close',
  globalCases: 'Global Cases for the Selected Date',
  aboutTheApp: 'About this App',
  about: 'About',
  viewHistoricalData: 'View historical data',
  ok: 'OK',
  filterStates: 'Filter states',
  noDataAvailableTitle: 'HTTP Error',
  filterCountries: 'Filter countries',
  viewDataForSelectedDate: 'View data for the selected day',
  todayCases: 'Daily Cases',
  todayDeaths: 'Daily Fatalities',
  critical: 'Critical',
  viewTodaysData: 'View today\'s data',
  externalLinksTitle: 'My other Vue projects',
  externalLinksSubTitle: 'Links to other personal Vue projects I\'ve built',
  еxportAsLabel: 'Export as:',
  filterCountriesByCfr: 'Filter countries',
  allBedsAvailable: 'All beds available',
  icuBedsAvailable: 'ICU beds available',
  geoError: `Could not find the geo coordinates of the location 
    specified or no data is available for the selected date. 
    Please, try another location from the menu on the left.`,
  cfrDescription: `Case Fatality Rate (CFR), also called Case Fatality Ratio, 
    in epidemiology, is the proportion of people who die from a specified disease 
    among all individuals diagnosed with the disease over a certain period of time. 
    Case fatality rate is calculated by dividing the number of deaths from a 
    specified disease over a defined period of time by the number of individuals 
    diagnosed with the disease during that time; the resulting ratio is then 
    multiplied by 100 to yield a percentage. The grid below shows affected countries 
    with CFR above 0.`,
  socialDistancingInfo: `<ul>
      <li>These dates pertain only to government-mandated measures implemented for 
      the entire population within the selected location.</li>
    </ul>`,
  noDataAvailableMessage: `The API servers do not supply any data at the moment.<br /><br />
    This is <strong class="red--text">not</strong> an issue with this app as we do not own or process the incoming data.<br /><br />
    Click <strong class="red--text">OK</strong> to reload the page or <strong class="red--text">try again later</strong>.`,
  hospitalResourceUseInInfo: `<ul>
        <li>The numbers for <strong>All beds needed</strong> and <strong>All beds available</strong> include ICU beds.</li>
        <li><strong>All beds available</strong> is the total number of hospital beds available for COVID patients minus the average historical bed use.</li>
        <li><strong>ICU beds available</strong> is the total number of ICU beds available for COVID patients minus the average historical ICU bed use.</li>
        <li><strong>Invasive ventilators needed</strong> does not account for the number of ventilators available (ventilator capacity data are not available at this time).</li>
    </ul>
  `,
  deathsPerDayInInfo: `<ul>
    <li>Since data can fluctuate daily, we use the overall trend (rather than the single 
    highest reported number) to identify a peak date of daily deaths.</li>
  </ul>`,
  loadingOverlayMessages: {
    compareDates: 'Getting historical data... (1/4)',
    getHistory: 'Getting available dates... (2/4)',
    getToday: 'Getting selected day data... (3/4)',
    getCoords: 'Getting geo coordinates... (4/4)',
    ready: 'Ready',
    getCountryInfo: 'Getting country details',
    getStatesData: 'Getting United States data...',
    gettingProjectionsLocations: 'Getting projections locations',
    gettingProjectionsForTheSelectedLocation: 'Getting projections for the selected location'
  },
  projectionsIcons: {
    '1': {
      icon: 'mdi-home',
      label: 'People instructed to stay at home'
    },
    '2': {
      icon: 'mdi-school',
      label: 'Educational facilities closed'
    },
    '3': {
      icon: 'mdi-store',
      label: 'Non-essential services (bars, restaurants) closed'
    },
    '4': {
      icon: 'mdi-comment-question-outline',
      label: 'Measure 4 not announced'
    },
    '5': {
      icon: 'mdi-airplane',
      label: 'Travel severely limited'
    },
    '6': {
      icon: 'mdi-comment-question-outline',
      label: 'Measure 6 not announced'
    },
    '7': {
      icon: 'mdi-comment-question-outline',
      label: 'Measure 7 not announced'
    },
    '8': {
      icon: 'mdi-test-tube',
      label: 'Date to switch from social distancing to testing/quarantine'
    },
    '9': {
      icon: 'mdi-nature-people',
      label: 'Limited large group gatherings'
    },
    '10': {
      icon: 'mdi-laptop',
      label: 'Partial business closure'
    }
  },
  /**
   * @function resourcesNeededOnPeakDate
   * @param {string} country [country = '']
   * @return {string}
   */
  resourcesNeededOnPeakDate: (country) => {
    return `Resources needed on peak date in <span class="capitalize">${country}</span>`
  },
  /**
   * @function deathsPerDayIn
   * @param {string} country [country = '']
   * @return {string}
   */
  deathsPerDayIn: (country = '') => {
    return `Deaths per day in <span class="capitalize">${country}</span>`
  },
  /**
   * @function peakDeathsMean
   * @param {number} number [number = '']
   * @return {string}
   */
  peakDeathsMean: (number = 0) => {
    return `${number} COVID-19 death${number === 1 ? '' : 's'}`
  },
  /**
   * @function peakDeathsMeanOn
   * @param {string} date [date = '']
   * @return {string}
   */
  peakDeathsMeanOn: (date = '') => {
    return `projected on ${date}`
  },
  /**
   * @function daysUntilPeakDate
   * @param {number} days [days = 0]
   * @return {string}
   */
  daysUntilPeakDate: (days = 0) => {
    return `${Math.abs(days)} day(s) ${(Math.sign(days) === -1 ? '<strong>since</strong>' : '<strong>until</strong>')} projected peak in daily deaths`
  },
  /**
   * @function hospitalResourceUseIn
   * @param {string} country [country = '']
   * @return {string}
   */
  hospitalResourceUseIn: (country = '') => {
    return `Hospital resource use in <span class="capitalize">${country}</span>`
  },
  /**
   * @function socialDistancing
   * @param {string} country [country = '']
   * @return {string}
   */
  socialDistancing: (country = '') => {
    return `Government-mandated social distancing in <span class="capitalize">${country}</span>`
  },
  /**
   * @function latestNewsAbout
   * @param {string} country [country = '']
   * @param {number} limit [country = null]
   * @return {string}
   */
  latestNewsAbout: (country = '', limit = null) => {
    return `Latest ${limit} News About the Coronavirus Spread in ${country}`
  },
  /**
   * @function exportAs
   * @param {string} format [format = 'csv']
   * @return {string}
   */
  exportAs: (format = 'csv') => {
    return `Export as .${format} file`
  },
  /**
   * @function versionLine
   * @return {string}
   */
  versionLine: () => {
    return `<a href="https://wemakesites.net" title="Developed by Martin Ivanov" target="_blank">Martin Ivanov</a> <span class="hidden-sm-and-down">&middot; <a title="Blog" href="https://martinivanov.net" target="_blank">Blog</a></span> &middot; <a title="${donate}" href="${config.donateUrl}" target="_blank">${donate}</a>`
  },
  /**
   * @function setColorScheme
   * @param {string} color
   * @return {string}
   */
  setColorScheme: (color) => {
    return `Set ${color} color scheme`
  },
  /**
   * @function setTheme
   * @param {boolean} dark
   * @return {string}
   */
  setTheme: (dark) => {
    return `Set ${dark ? 'light' : 'dark'} theme`
  },
  /**
   * @function totalCases
   * @param {number} cases [cases = null]
   * @return {string}
   */
  totalCases: (cases = null) => {
    return `${numeral(cases).format(numeralsFormat)} global cases`
  },
  /**
   * @function totalCases
   * @param {string} country [country = '']
   * @param {number} cases [cases = null]
   * @return {string}
   */
  totalCasesByCountry: (country = '', cases = null) => {
    return `${numeral(cases).format(numeralsFormat)} confirmed cases in ${country}`
  },
  /**
   * @function viewCountryData
   * @param {string} country [country = '']
   * @return {string}
   */
  viewCountryData (country = '') {
    return `View more information about ${country}`
  },
  /**
   * @function countriesListSubTitle
   * @param {string} country [country = '']
   * @return {string}
   */
  countriesListSubTitle (country = '') {
    const delimiter = '&middot;'
    return `<span title="${confirmedCases}">${numeral(country.cases).format(numeralsFormat)}</span> ${delimiter} <span title="${recovered}" class="green--text">${numeral(country.recovered).format(numeralsFormat)}</span> ${delimiter} <span title="${deaths}" class="red--text">${numeral(country.deaths).format(numeralsFormat)}</span>`
  }
}

export default lang
