<?php
/**
 * API routes
 */

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, OPTIONS, DELETE, PUT, GET');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization');

ob_start('ob_gzhandler');

$request = $_SERVER['REQUEST_URI'];
$requestHost = $_SERVER['HTTP_HOST'];

require_once 'api.php';
$routes = json_decode(file_get_contents('./routes.json'), true);
$apiRoot = '/api/';

if (strpos($requestHost, 'localhost') !== false) {
    $apiRoot = '/covid-wemakesites-net/api/';
}

$api = new Api();

switch ($request) {
    case $apiRoot . $routes['routes']['url']:
        $api -> routes();
        break;
    case $apiRoot . $routes['coords']['url']:
        $api -> coords();
        break;
    case $apiRoot . $routes['history']['url']:
        $api -> history();
        break;
    case $apiRoot . $routes['today']['url']:
        $api -> today();
        break;
    case $apiRoot . $routes['date']['url']:
        $api -> date();
        break;
    case $apiRoot . $routes['compare/dates']['url']:
        $api -> compareDates();
        break;
    case $apiRoot . $routes['geoip']['url']:
        $api -> geoip();
        break;
    case $apiRoot . $routes['news']['url']:
        $api -> news();
        break;
    case $apiRoot . $routes['country']['url']:
        $api -> country();
        break;
    case $apiRoot . $routes['states']['url']:
        $api -> states();
        break;
    case $apiRoot . $routes['state']['url']:
        $api -> state();
        break;
    case $apiRoot . $routes['projections']['url']:
        $api -> projections();
        break;
    case $apiRoot . $routes['location']['url']:
        $api -> location();
        break;
}