<?php

/**
 * @class Api
 * @author Martin Ivanov
 */
class Api {

    static function location () {
        $data = curl_init();
        curl_setopt($data, CURLOPT_URL, "https://covid19.healthdata.org/api/metadata/location");
        curl_setopt($data, CURLOPT_RETURNTRANSFER, true);
        $state_data = curl_exec($data);
        if (curl_exec($data) === false) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'curl failed'
            ));
        } else {
            echo $state_data;
            curl_close($data);
        }
    }

    static function projectionsUrl ($verb, $id) {
        return 'https://covid19.healthdata.org/api/data/' . $verb . '?location=' . $id;
    }

    static function projections () {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $id = json_decode(file_get_contents('php://input'), true)['id'];
            if (!$id) {
                die(json_encode(array(
                    'status' => 'error',
                    'message' => 'id parameter not provided in request body'
                )));
            }
            $response_data = array();
            // hospitalization
            $hospitalization = curl_init();
            $hospitalization_verb = 'hospitalization';
            $hospitalization_url = API::projectionsUrl($hospitalization_verb, $id);
            curl_setopt($hospitalization, CURLOPT_URL, $hospitalization_url);
            curl_setopt($hospitalization, CURLOPT_RETURNTRANSFER, true);
            $hospitalization_data = curl_exec($hospitalization);
            if (curl_exec($hospitalization) === false) {
                die(json_encode(array(
                    'status' => 'error',
                    'message' => 'curl failed'
                )));
            } else {
                $response_data[$hospitalization_verb] = json_decode($hospitalization_data);
                curl_close($hospitalization);
                // intervention
                $intervention = curl_init();
                $intervention_verb = 'intervention';
                $intervention_url = API::projectionsUrl($intervention_verb, $id);
                curl_setopt($intervention, CURLOPT_URL, $intervention_url);
                curl_setopt($intervention, CURLOPT_RETURNTRANSFER, true);
                $intervention_data = curl_exec($intervention);
                if (curl_exec($intervention) === false) {
                    die(json_encode(array(
                        'status' => 'error',
                        'message' => 'curl failed'
                    )));
                } else {
                    $response_data[$intervention_verb] = json_decode($intervention_data);
                    curl_close($intervention);
                    // peak_death
                    $peak_death = curl_init();
                    $peak_death_verb = 'peak_death';
                    $peak_death_url = API::projectionsUrl($peak_death_verb, $id);
                    curl_setopt($peak_death, CURLOPT_URL, $peak_death_url);
                    curl_setopt($peak_death, CURLOPT_RETURNTRANSFER, true);
                    $peak_death_data = curl_exec($peak_death);
                    if (curl_exec($peak_death) === false) {
                        die(json_encode(array(
                            'status' => 'error',
                            'message' => 'curl failed'
                        )));
                    } else {
                        $response_data[$peak_death_verb] = json_decode($peak_death_data);
                        curl_close($peak_death);
                        // bed
                        $bed = curl_init();
                        $bed_verb = 'bed';
                        $bed_url = API::projectionsUrl($bed_verb, $id);
                        curl_setopt($bed, CURLOPT_URL, $bed_url);
                        curl_setopt($bed, CURLOPT_RETURNTRANSFER, true);
                        $bed_data = curl_exec($bed);
                        if (curl_exec($bed) === false) {
                            die(json_encode(array(
                                'status' => 'error',
                                'message' => 'curl failed'
                            )));
                        } else {
                            $response_data[$bed_verb] = json_decode($bed_data);
                            curl_close($bed);
                            echo json_encode($response_data);
                        }
                    }
                }
            }
        } else {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'only POST method is allowed'
            ));
        }
    }

    static function state () {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $state = json_decode(file_get_contents('php://input'), true)['state'];
            if (!$state) {
                die(json_encode(array(
                    'status' => 'error',
                    'message' => 'state parameter not provided in request body'
                )));
            }
            $data = curl_init();
            curl_setopt($data, CURLOPT_URL, "https://corona.lmao.ninja/v2/states/" . strtolower($state));
            curl_setopt($data, CURLOPT_RETURNTRANSFER, true);
            $state_data = curl_exec($data);
            if (curl_exec($data) === false) {
                echo json_encode(array(
                    'status' => 'error',
                    'message' => 'curl failed'
                ));
            } else {
                echo $state_data;
                curl_close($data);
            }
        } else {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'only POST method is allowed'
            ));
        }
    }

    static function states () {
        $data = curl_init();
        curl_setopt($data, CURLOPT_URL, "https://corona.lmao.ninja/v2/states");
        curl_setopt($data, CURLOPT_RETURNTRANSFER, true);
        $state_data = curl_exec($data);
        if (curl_exec($data) === false) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'curl failed'
            ));
        } else {
            echo $state_data;
            curl_close($data);
        }
    }

    static function country () {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $country = json_decode(file_get_contents('php://input'), true)['country'];
            if (!$country) {
                die(json_encode(array(
                    'status' => 'error',
                    'message' => 'country parameter not provided in request body'
                )));
            }
            $data = curl_init();
            curl_setopt($data, CURLOPT_URL, "https://restcountries.eu/rest/v2/name/" . strtolower($country));
            curl_setopt($data, CURLOPT_RETURNTRANSFER, true);
            $country_data = curl_exec($data);
            if (curl_exec($data) === false) {
                echo json_encode(array(
                    'status' => 'error',
                    'message' => 'curl failed'
                ));
            } else {
                echo $country_data;
                curl_close($data);
            }
        } else {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'only POST method is allowed'
            ));
        }
    }

    static function news () {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $country = json_decode(file_get_contents('php://input'), true)['country'];
            if (!$country) {
                die(json_encode(array(
                    'status' => 'error',
                    'message' => 'country parameter not provided in request body'
                )));
            }
            $news = curl_init();
            curl_setopt($news, CURLOPT_URL, "https://news.google.com/rss/search?q=covid+" . $country . "&hl=en-SG&gl=SG&ceid=SG%3Aen");
            curl_setopt($news, CURLOPT_RETURNTRANSFER, true);
            $news_data = curl_exec($news);
            if (curl_exec($news) === false) {
                echo json_encode(array(
                    'status' => 'error',
                    'message' => 'curl failed'
                ));
            } else {
                $news_data = str_replace(array("\n", "\r", "\t"), '', $news_data);
                $news_data = trim(str_replace('"', "'", $news_data));
                $simpleXml = simplexml_load_string($news_data);
                $news_data = json_encode($simpleXml);
                curl_close($news);
                echo $news_data;
            }
        } else {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'only POST method is allowed'
            ));
        }
    }

    static function geoip () {
        $geoip = curl_init();
        curl_setopt($geoip, CURLOPT_URL, "http://ip-api.com/json/");
        curl_setopt($geoip, CURLOPT_RETURNTRANSFER, true);
        $geoip_data = curl_exec($geoip);
        if (curl_exec($geoip) === false) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'curl failed'
            ));
        } else {
            curl_close($geoip);
            echo $geoip_data;
        }
    }

    static function routes () {
        echo file_get_contents('./routes.json');
    }

    static function coords () {
        echo file_get_contents('./data/coords.json');
    }

    static function history () {
        echo json_encode(array_slice(scandir('./dates'), 2));
    }

    static function compareDates () {
        $dates = array_slice(scandir('./dates'), 2);
        $data = (object) [];
        foreach($dates as $date) {
            $data -> $date = json_decode(file_get_contents('./dates/' . $date . '/all.json'), true);
        }
        echo json_encode($data);
    }

    static function date () {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $date = json_decode(file_get_contents('php://input'), true)['date'];
            $folder = './dates/' . $date;
            if ($date && is_dir($folder)) {
                echo json_encode(array(
                    'all' => json_decode(file_get_contents($folder . '/all.json'), true),
                    'countries' => json_decode(file_get_contents($folder . '/countries.json'), true)
                ));
            } else {
                echo json_encode(array(
                    'status' => 'error',
                    'message' => 'no data available for ' . $date
                ));
            }
        } else {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'only POST method is allowed'
            ));
        }
    }

    static function today () {
        $date = date('Y-m-d');
        $folder = './dates/' . $date;
    
        $all = curl_init();
        curl_setopt($all, CURLOPT_URL, "https://corona.lmao.ninja/v2/all");
        curl_setopt($all, CURLOPT_RETURNTRANSFER, true);
        $all_data = curl_exec($all);

        $countries = curl_init();
        curl_setopt($countries, CURLOPT_URL, "https://corona.lmao.ninja/v2/countries");
        curl_setopt($countries, CURLOPT_RETURNTRANSFER, true);
        $countries_data = curl_exec($countries);
        if (curl_exec($all) === false && curl_exec($countries) === false) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'curl failed'
            ));
        } else {
            if (!is_dir($folder)) {
                mkdir($folder);
            }
            $countries_data = json_decode($countries_data);
            $all_data = json_decode($all_data);
            if (!empty($countries_data) && !empty((array)$all_data)) {
                file_put_contents($folder . '/countries.json', json_encode($countries_data));
                file_put_contents($folder . '/all.json', json_encode($all_data));
                echo json_encode(array(
                    'status' => 'success',
                    'message' => 'data synced'
                ));
            } else {
                echo json_encode(array(
                    'status' => 'error',
                    'message' => 'insufficient or corrupt data'
                ));
            }
        }
        curl_close($all);
        curl_close($countries);
    }
}